package fit5042.assignment.controllers;

import javax.el.ELContext;


import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.Customer;
import fit5042.assignment.mbeans.CustomerManagedBean;

import javax.faces.bean.ManagedProperty;


@RequestScoped
@Named("addCustomer")
public class AddCustomer {

    @ManagedProperty(value = "#{customerManagedBean}") 
    CustomerManagedBean customerManagedBean;

    private boolean showForm = true;
    private Customer customer;
    AUSPrinting app;
    private int customerid;
	private ContactPerson contactPerson;
    

    public boolean isShowForm() {
        return showForm;
    }

    public AddCustomer() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (AUSPrinting) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "AUSPrinting");

        customerManagedBean = (fit5042.assignment.mbeans.CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(context, null, "customerManagedBean");
        contactPerson = new ContactPerson();
		customerid = Integer.valueOf(
				FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("customerID"));
		setCustomer(app.getCustomerByID(customerid));
    }

    public void addCustomer(Customer customer) {
        try {
            
            customerManagedBean.addCustomer(customer);

         
             app.searchAll();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been added"));
        } catch (Exception ex) {

        }
        showForm = true;
    }
    
    public int getCustomerid() {
		return customerid;
	}

	
	public void setCustomerid(int customerid) {
		this.customerid = customerid;
	}

	
	public Customer getCustomer() {
		return customer;
	}

	
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	
	public ContactPerson getContactPerson() {
		return contactPerson;
	}

	
	public void setContactPerson(ContactPerson contactPerson) {
		this.contactPerson = contactPerson;
	}


}
