package fit5042.assignment.controllers;

import javax.el.ELContext;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.repository.entities.Customer;
/**
*
* @author Andong Yang 30054605
*
*/
@RequestScoped
@Named("searchCustomer")
public class SearchCustomer {
	private Customer customer;
	AUSPrinting app;
	int searchByInt;
	String searchByString;
	private boolean showForm;

	/**
	 * @param customer
	 * @param ausPrinting
	 */
	public SearchCustomer() {
		ELContext context = FacesContext.getCurrentInstance().getELContext();

		app = (AUSPrinting) FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(context,
				null, "ausPrinting");

		app.updateCustomerList();
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/**
	 * @return the app
	 */
	public AUSPrinting getApp() {
		return app;
	}
	
	public boolean isShowForm() {
        return showForm;
    }

	/**
	 * @param app the app to set
	 */
	public void setApp(AUSPrinting app) {
		this.app = app;
	}
	
    public void searchAll() {
        try {
            app.searchAll();
        } catch (Exception ex) {
        	

        }
        
    }
    
    public void searchCustomerById(int customerId) {
        try {
            //search this property then refresh the list in PropertyApplication bean
            app.searchCustomerById(customerId);
        } catch (Exception ex) {

        }
        showForm = true;

    }
    
    public int getSearchByInt() {
        return searchByInt;
    }
    public void setSearchByInt(int newInt) {
        this.searchByInt = newInt;
    }
    
    
    public String  getSearchByString() {
    	return searchByString;
    }
    

}
