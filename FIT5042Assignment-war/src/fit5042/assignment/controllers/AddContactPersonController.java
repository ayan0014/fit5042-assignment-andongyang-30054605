package fit5042.assignment.controllers;

import java.io.Serializable;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.el.ELContext;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.mbeans.ContactPersonManagedBean;
import fit5042.assignment.mbeans.CustomerManagedBean;
import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.Customer;
/**
*
* @author Andong Yang 30054605
*
*/
@ConversationScoped
@Named("addContactPerson")
public class AddContactPersonController implements Serializable {
	
	@ManagedProperty(value = "#{contactPersonManagedBean}")
	ContactPersonManagedBean contactPersonManagedBean;
	
	private AUSPrinting app;
	private int customerid;
	private Customer customer;
	private ContactPerson contactPerson;

	public AddContactPersonController() {
		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		contactPersonManagedBean = (ContactPersonManagedBean) FacesContext.getCurrentInstance().getApplication().getELResolver()
				.getValue(elContext, null, "contactPersonManagedBean");
		app = (AUSPrinting) FacesContext.getCurrentInstance().getApplication().getELResolver()
				.getValue(elContext, null, "ausPrinting");
		contactPerson = new ContactPerson();
		customerid = Integer.valueOf(
				FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("customerID"));
		setCustomer(app.getCustomerByID(customerid));
	}
	
	public void addContactPerson() {
			contactPerson.setCustomer(getCustomer());
			contactPersonManagedBean.addContactPerson(contactPerson);
			app.searchAll();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Contact Person has been added"));
	}

	
	public int getCustomerid() {
		return customerid;
	}

	
	public void setCustomerid(int customerid) {
		this.customerid = customerid;
	}

	
	public Customer getCustomer() {
		return customer;
	}

	
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	
	public ContactPerson getContactPerson() {
		return contactPerson;
	}

	
	public void setContactPerson(ContactPerson contactPerson) {
		this.contactPerson = contactPerson;
	}

	
}
