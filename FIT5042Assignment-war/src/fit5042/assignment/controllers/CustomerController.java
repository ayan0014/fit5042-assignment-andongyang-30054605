package fit5042.assignment.controllers;

import java.util.ArrayList;




import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.Customer;
/**
*
* @author Andong Yang 30054605
*
*/
@Named(value = "customerController")
@RequestScoped
public class CustomerController {

	private int customerIndex;
	private Customer customer;

	private ArrayList<String> industries;
	private String selectedIndustry;
	
	public CustomerController() {
		
		customerIndex = Integer.valueOf(
				FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("customerIndex"));
		customer = getCustomer();
		industries = getIndustries();
		setSelectedIndustry("");
	}

	/**
	 * @return the customerIndex
	 */
	public int getCustomerIndex() {
		return customerIndex;
	}

	/**
	 * @param customerIndex the customerIndex to set
	 */
	public void setCustomerIndex(int customerIndex) {
		this.customerIndex = customerIndex;
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		if (customer == null) {
			ELContext context = FacesContext.getCurrentInstance().getELContext();
			AUSPrinting app = (AUSPrinting) FacesContext.getCurrentInstance().getApplication().getELResolver()
					.getValue(context, null, "ausPrinting");
			
			return app.getCustomerByID(customerIndex);
		}
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public ArrayList<String> getIndustries() {
		return new ArrayList<String>() {
			{
				add("Building");
				add("Data Communication");
				add("IT");
				add("Farm");
				add("Education");
				add("Bank");
			}
		};
	}

	public void setIndustries(ArrayList<String> industries) {
		this.industries = industries;
	}

	public String getSelectedIndustry() {
		return selectedIndustry;
	}

	public void setSelectedIndustry(String selectedIndustry) {
		this.selectedIndustry = selectedIndustry;
	}

}
