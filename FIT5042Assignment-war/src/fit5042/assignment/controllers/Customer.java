package fit5042.assignment.controllers;

import fit5042.assignment.repository.entities.ContactPerson;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.inject.Named;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.enterprise.context.Dependent;

/**
 *
 * @author Andong Yang
 */

@Named(value = "customer")
public class Customer implements Serializable {

	private int customerID;
	private String firstName;
	private String lastName;
//	private Set<ContactPerson> contacts;
//	public Set<String> tags;
//	
//	private int conactPersonId;
//	private String name;
//	private String phoneNumber;
//	private Customer customer;

	
	public Customer() {
		 //this.tags = new HashSet<>();
	}
	
	
	public Customer(int customerID, String firstName, String lastName) {
		this.customerID = customerID;
		this.firstName = firstName;
		this.lastName = lastName;
		//contacts = new HashSet<>();
		
	}

	
	public int getCustomerID() {
		return customerID;
	}

	
	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	
	public String getFirstName() {
		return firstName;
	}

	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	
	public String getLastName() {
		return lastName;
	}

	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
//    public Set<String> getTags() {
//        return tags;
//    }
//
//    public void setTags(Set<String> tags) {
//        this.tags = tags;
//    }
//	
//
//	
//	
//	public Set<ContactPerson> getContacts() {
//		return contacts;
//	}
//
//	
//	public void setContacts(Set<ContactPerson> contacts) {
//		this.contacts = contacts;
//	}
//	
//	
//	

	@Override
	public String toString() {
		return "Customer [customerID=" + customerID + ", firstName=" + firstName + ", lastName=" + lastName + "]";
	}

//	public int getConactPersonId() {
//		return conactPersonId;
//	}
//
//	public void setConactPersonId(int conactPersonId) {
//		this.conactPersonId = conactPersonId;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public String getPhoneNumber() {
//		return phoneNumber;
//	}
//
//	public void setPhoneNumber(String phoneNumber) {
//		this.phoneNumber = phoneNumber;
//	}
//
//	public Customer getCustomer() {
//		return customer;
//	}
//
//	public void setCustomer(Customer customer) {
//		this.customer = customer;
//	}
	
}