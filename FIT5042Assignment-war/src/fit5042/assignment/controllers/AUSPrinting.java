package fit5042.assignment.controllers;

import java.util.ArrayList;


import javax.el.ELContext;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.mbeans.CustomerManagedBean;
import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.Customer;


/**
*
* @author Andong Yang 30054605
*
*/
@Named(value = "ausPrinting")
@ApplicationScoped
public class AUSPrinting {
    @ManagedProperty(value = "#{customerManagedBean}")
    CustomerManagedBean customerManagedBean;
    
    private ArrayList<Customer> customers;
    
    // Add some customer data from db to app 
    public AUSPrinting() throws Exception {
    	customers = new ArrayList<>();
        //instantiate customerManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "customerManagedBean");

        //get properties from db 
        updateCustomerList();
        
    }
    
    public void updateCustomerList() {
        if (customers != null && customers.size() > 0)
        {
            
        }
        else
        {
        	customers.clear();

            for (Customer customer : customerManagedBean.getAllCustomers())
            {
            	customers.add(customer);
            }

            setCustomers(customers);
        }
    }
    
    
    public Customer getCustomerByID(int customerID) {
    	Customer customer = new Customer();
    	for(Customer cus:getCustomers()) {
    		if (cus.getCustomerID() == customerID) {
    			return cus;
    		}
    	}
		return customer;
    }
    
    
	/**
	 * @return the customers
	 */
	public ArrayList<Customer> getCustomers() {
		updateCustomerList();
		return customers;
	}

	/**
	 * @param customers the customers to set
	 */
	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}
    
    public void searchAll()
    {
    	customers.clear();
        
        for (Customer customer : customerManagedBean.getAllCustomers())
        {
        	customers.add(customer);
        }
        
        setCustomers(customers);
    }

	public void searchCustomerById(int customerId) {
		// TODO Auto-generated method stub
		customers.clear();

        customers.add(customerManagedBean.searchCustomerById(customerId));
	}
	
    
    
}
