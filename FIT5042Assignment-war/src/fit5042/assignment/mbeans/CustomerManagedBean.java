package fit5042.assignment.mbeans;

import java.io.Serializable;


import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import fit5042.assignment.repository.CustomerRepository;
import fit5042.assignment.repository.entities.Customer;

/**
*
* @author Andong Yang 30054605
*
*/
@ManagedBean(name = "customerManagedBean")
@SessionScoped
public class CustomerManagedBean implements Serializable{
	
    @EJB
    CustomerRepository customerRepository;
    
    public CustomerManagedBean() {
    }
	
    public List<Customer> getAllCustomers() {
        try {
            List<Customer> customers = customerRepository.getAllCustomers();
            return customers;
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void editCustomer(Customer customer) {
            String fname = customer.getFirstName();
            String lname = customer.getLastName();
            customer.setFirstName(fname);
            customer.setLastName(lname);
            try {
				customerRepository.editCustomer(customer);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer's detail has been updated"));
        
    }

	public void removeCustomer(int customerId) {
		// TODO Auto-generated method stub
		 try {
	            customerRepository.removeCustomer(customerId);
	        } catch (Exception ex) {
	            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
	        }
	}

	  public void addCustomer(Customer localCustomer) {
		// TODO Auto-generated method stub
		  try {
	            customerRepository.addCustomer(localCustomer);
	        } catch (Exception ex) {
	            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
	        }
	}

	public Customer searchCustomerById(int customerId) {
		// TODO Auto-generated method stub
		try {
            return customerRepository.searchCustomerById(customerId);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
		
	}

}
