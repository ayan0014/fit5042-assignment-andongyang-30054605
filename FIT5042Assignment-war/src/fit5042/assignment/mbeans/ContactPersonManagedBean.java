package fit5042.assignment.mbeans;

import java.io.Serializable;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fit5042.assignment.repository.ContactPersonRepository;
import fit5042.assignment.repository.CustomerRepository;
import fit5042.assignment.repository.entities.ContactPerson;
/**
*
* @author Andong Yang 30054605
*
*/
@ManagedBean(name = "contactPersonManagedBean")
@SessionScoped
public class ContactPersonManagedBean implements Serializable {

	
	@EJB
    ContactPersonRepository contactPersonRepository;
	
	public ContactPersonManagedBean() {		
	}

    public void addContactPerson(ContactPerson contactPerson) {
    	try {
    		contactPersonRepository.addContactPerson(contactPerson);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
	
}
