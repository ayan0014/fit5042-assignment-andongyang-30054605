package fit5042.assignment.repository.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2020-11-04T15:21:03.677+1100")
@StaticMetamodel(ContactPerson.class)
public class ContactPerson_ {
	public static volatile SingularAttribute<ContactPerson, Integer> conactPersonId;
	public static volatile SingularAttribute<ContactPerson, String> phoneNumber;
	public static volatile SingularAttribute<ContactPerson, Customer> customer;
	public static volatile SingularAttribute<ContactPerson, String> name;
}
