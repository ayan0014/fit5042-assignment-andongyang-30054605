package fit5042.assignment.repository;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.Customer;
/**
*
* @author Andong Yang 30054605
*
*/
@Stateless
public class CustomerRepositorySessionBean implements CustomerRepository {

	@PersistenceContext (unitName = "FIT5042Assignment-ejb")//unitName = persistence-unit in jebModule/META-INF/persistence.xml
	private EntityManager entityManager;
	
	@Override
	public List<Customer> getAllCustomers() throws Exception {
		return entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();	
	}

	@Override
	public void editCustomer(Customer customer) throws Exception {
		// TODO Auto-generated method stub
		if(customer != null){
			entityManager.merge(customer);
		}		
	}
	
	public void removeCustomer(int customerID) throws Exception{
		List<Customer> customers = entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
		for(Customer c : customers) {
			if(c.getCustomerID() == customerID) {
				entityManager.remove(c);
			}
		}
	}
	
	public void addCustomer(Customer c) throws Exception{
		List<Customer> customers = entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
		c.setCustomerID(customers.get(0).getCustomerID()+1);
		entityManager.persist(c);
	}
	
	public Customer searchCustomerById(int customerId) throws Exception{
		
		Customer customer = entityManager.find(Customer.class, customerId);
        customer.getTags();
        return customer;
	}


}
