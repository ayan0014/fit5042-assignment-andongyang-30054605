package fit5042.assignment.repository;

import java.util.List;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.Customer;

/**
*
* @author Andong Yang 30054605
*
*/

@Stateless
public class ContactPersonRepositorySessionBean implements ContactPersonRepository {

	@PersistenceContext(unitName = "FIT5042Assignment-ejb") 
	private EntityManager entityManager;

	@Override
	public void addContactPerson(ContactPerson contactPerson) throws Exception {
		List<ContactPerson> contactPersons = entityManager.createNamedQuery(ContactPerson.GET_ALL_CONTACT_PERSON)
				.getResultList();
		contactPerson.setConactPersonId(contactPersons.get(0).getConactPersonId() + 1);
		Customer customer = contactPerson.getCustomer();
		customer.getContacts().add(contactPerson);
		entityManager.merge(customer);
	}
}
