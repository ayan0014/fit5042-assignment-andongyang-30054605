package fit5042.assignment.repository;

import java.util.List;


import javax.ejb.Remote;
import com.sun.xml.internal.ws.api.pipe.Tube;
import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.Customer;


/**
*
* @author Andong Yang 30054605
*
*/

@Remote
public interface CustomerRepository {
	
    public List<Customer> getAllCustomers() throws Exception;
    public void editCustomer(Customer customer) throws Exception;
	public void removeCustomer(int customerID) throws Exception;
	public void addCustomer(Customer c) throws Exception;
	public Customer searchCustomerById(int customerId) throws Exception;
		

}
