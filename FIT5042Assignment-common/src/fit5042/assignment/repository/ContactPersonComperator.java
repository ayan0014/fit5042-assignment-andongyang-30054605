package fit5042.assignment.repository;

import java.util.Comparator;
import fit5042.assignment.repository.entities.ContactPerson;

/**
*
* @author Andong Yang 30054605
*
*/
public class ContactPersonComperator implements Comparator<ContactPerson> {
    @Override
    public int compare(ContactPerson c1, ContactPerson c2) {
        return c1.getConactPersonId() - c2.getConactPersonId();
    }
}
