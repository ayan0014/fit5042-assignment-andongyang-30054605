package fit5042.assignment.repository;

import javax.ejb.Remote;
import fit5042.assignment.repository.entities.ContactPerson;

/**
*
* @author Andong Yang 30054605
*
*/


@Remote
public interface ContactPersonRepository {

    public void addContactPerson(ContactPerson contactPerson) throws Exception;

}
