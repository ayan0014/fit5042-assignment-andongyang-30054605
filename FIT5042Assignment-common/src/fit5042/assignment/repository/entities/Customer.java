package fit5042.assignment.repository.entities;

import java.io.Serializable;



import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import fit5042.assignment.repository.ContactPersonComperator;


/**
*
* @author Andong Yang 30054605
*
*/
@Entity
@NamedQueries({
	@NamedQuery(name = Customer.GET_ALL_QUERY_NAME, query = "SELECT c FROM Customer c order by c.customerID desc") })
public class Customer  implements Serializable{

	public static final String GET_ALL_QUERY_NAME =  "Customer.getAll";
	
	private int customerID;
	private String firstName;
	private String lastName;
	private Set<ContactPerson> contacts;
	public Set<String> tags;
	
	

	
	public Customer() {
		 this.tags = new HashSet<>();
	}
	
	/**
	 * @param customerID
	 * @param firstName
	 * @param lastName
	 */
	public Customer(int customerID, String firstName, String lastName) {
		this.customerID = customerID;
		this.firstName = firstName;
		this.lastName = lastName;
		contacts = new HashSet<>();
	}

	/**
	 * @return the customerID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "customer_id")
	public int getCustomerID() {
		return customerID;
	}

	/**
	 * @param customerID the customerID to set
	 */
	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@CollectionTable(name="TAG")
    @Column(name="VALUE")
    @ElementCollection(fetch=FetchType.EAGER)
    public Set<String> getTags() {
        return tags;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }
	

	/**
	 * @return the contacts
	 */
	@OneToMany(mappedBy = "customer",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@OrderBy
	public Set<ContactPerson> getContacts() {
		return contacts;
	}

	/**
	 * @param contacts the contacts to set
	 */
	public void setContacts(Set<ContactPerson> contacts) {
		this.contacts = contacts;
	}

	@Override
	public String toString() {
		return "Customer [customerID=" + customerID + ", firstName=" + firstName + ", lastName=" + lastName + "]";
	}
	
}
