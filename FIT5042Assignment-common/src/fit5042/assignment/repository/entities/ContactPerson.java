package fit5042.assignment.repository.entities;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

/**
*
* @author Andong Yang 30054605
*
*/
@Entity
@Table(name = "CONTACT_PERSON")
@NamedQueries({ @NamedQuery(name = ContactPerson.GET_ALL_CONTACT_PERSON, query = "SELECT c FROM ContactPerson c order by c.conactPersonId desc") })
public class ContactPerson implements Serializable {

	public static final String GET_ALL_CONTACT_PERSON = "ContactPerson.getAll";

	private int conactPersonId;
	private String name;
	private String phoneNumber;
	private Customer customer;

	public ContactPerson() {
	}

	public ContactPerson(int conactPersonId, String name, String phoneNumber, Customer customer) {
		this.conactPersonId = conactPersonId;
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.customer = customer;
	}

	@Id
	@GeneratedValue
	@Column(name = "contact_person_id")
	public int getConactPersonId() {
		return conactPersonId;
	}

	public void setConactPersonId(int conactPersonId) {
		this.conactPersonId = conactPersonId;
	}

	@Email(message = "This is not a valid Email address")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "phone_number")
	@NotEmpty(message = "Phone number should not be empty")
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	

	/**
	 * @return the customer
	 */
	@ManyToOne(cascade = CascadeType.ALL)
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public boolean equals(Object i) {
		if (i == null) {
			return false;
		}
		if (getClass() != i.getClass()) {
			return false;
		}
		final ContactPerson other = (ContactPerson) i;
		if (this.conactPersonId != other.conactPersonId) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return this.conactPersonId + " - " + name + " - " + phoneNumber  + "-" + customer.getCustomerID();
	}
	
}
